package com.crecolto.lib;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class androidusb {

    private Socket socket;
    private PrintWriter out;
    private Scanner sc;
//    private BufferedOutputStream outputStream;


    /**
     * Initialize connection to the phone
     *
     */
    public void initializeConnection(){
        //Create socket connection
        try{
            socket = new Socket("localhost", 18888);
            out = new PrintWriter(
                    new BufferedWriter(new OutputStreamWriter(
                            socket.getOutputStream(), "UTF-8")), true);
//            out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "EUC_KR"), true);
//            outputStream = new BufferedOutputStream(socket.getOutputStream());

            /*pstream = new PrintWriter(new OutputStreamWriter(
                    csocket.getOutputStream(), StandardCharsets.UTF_8), true)*/

            //in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            sc = new Scanner(socket.getInputStream());

            // add a shutdown hook to close the socket if system crashes or exists unexpectedly
            Thread closeSocketOnShutdown = new Thread() {
                public void run() {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };

            Runtime.getRuntime().addShutdownHook(closeSocketOnShutdown);

        } catch (UnknownHostException e) {
            System.err.println("Socket connection problem (Unknown host)" + e.getStackTrace());
        } catch (IOException e) {
            System.err.println("Could not initialize I/O on socket " + e.getStackTrace());
        }
    }

    public static void main(String[] args) throws IOException {

        androidusb t = new androidusb();
        t.initializeConnection();

        boolean tf = false;

        if(!tf) {

            /*String originalStr = "테스트";
            String [] charSet = {"utf-8","euc-kr","ksc5601","iso-8859-1","x-windows-949", "utf-16"};

            for (int i=0; i<charSet.length; i++) {
                for (int j=0; j<charSet.length; j++) {
                    try {
                        System.out.println("[" + charSet[i] +"," + charSet[j] +"] = " + new String(originalStr.getBytes(charSet[i]), charSet[j]));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }*/

            String data="{\"user\":{\"user_name\":\"홍길동\",\"user_code\":\"1234567890\",\"price\":\"1004\",\"type\":\"7\",\"team\": \"부산교구 해운대 성당\"}}";

//            String data = "한글테스트";

            t.out.write(data);

//            t.out.write("{\"user\":{\"user_name\":\"홍길동\",\"user_code\":\"1234567890\",\"price\":\"1004\",\"type\":\"7\",\"team\": \"부산교구 해운대 성당\"}}");
            t.out.flush();
            tf = true;

            /*BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            String line;
            while(true){
                try {
                    if (!((line = br.readLine()) != null)) break;
                    System.out.println(line);
                    t.out.println(line);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }*/

            t.out.close();
//            br.close();
        }
    }

}
